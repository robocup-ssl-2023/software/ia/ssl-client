from distutils.cmd import Command
import rclpy
from rclpy.node import Node

from ssl_interfaces.msg import Data, Commands, Command

is_yellow = False
topic_data = "/yellow/data" if is_yellow else "/blue/data"
commands_data = "/yellow/commands" if is_yellow else "/blue/commands"


class MainNode(Node):
    def __init__(self):
        super().__init__("minimal_subscriber")
        self.subscription = self.create_subscription(
            Data, topic_data, self.listener_callback, 10
        )
        self.subscription  # prevent unused variable warning
        self.publisher_ = self.create_publisher(Commands, commands_data, 10)

    def listener_callback(self, msg):
        self.get_logger().info('I heard: "%s"' % msg.allies[1])
        self.get_logger().info

        msg = Commands()
        cmd = Command()

        cmd.velocity.linear.x = 1.0
        cmd.velocity.angular.z = 1.0
        msg.commands.append(cmd)

        self.publisher_.publish(msg)


def main(args=None):
    rclpy.init(args=args)

    node = MainNode()

    rclpy.spin(node)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    node.destroy_node()
    rclpy.shutdown()


if __name__ == "__main__":
    main()
